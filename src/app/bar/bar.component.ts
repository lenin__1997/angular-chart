import { Component } from '@angular/core';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent {
  public chart:any
  ngOnInit(){
    this.chart=new Chart("newchart",{
      type:'bar',
      data:{
        labels:["violet","Monday","Tuesday","wednesday","j","jj","ads","fdsf","sd","fds","fsa","rw","gy","jk"],
        datasets:[{
          label:"day",
          data:[0,10,15,20,4,5,2,54,67,12,32,34,9,13],
          backgroundColor:['red','blue',"green","violet"],
          borderWidth:1
        }]
      },
      options:{
        aspectRatio:5
      }
    });
  }
  
}
