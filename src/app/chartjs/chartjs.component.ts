import { Component, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-chartjs',
  templateUrl: './chartjs.component.html',
  styleUrls: ['./chartjs.component.css']
})
export class ChartjsComponent {
  public chart:any
  public chart1:any
  ngOnInit(){
     this.chart = new Chart('MyChart',{
      type: 'bar',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 6, 5, 8, 3],
          backgroundColor: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          borderWidth: 1
        }]
      },
      options: {
        aspectRatio:2.5
      }
    });
  }

  
  
  
  

}

